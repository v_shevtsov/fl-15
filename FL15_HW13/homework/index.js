function getAge(date) {
    const unixZeroYear = 1970;
    let currentDate = new Date(2020, 9, 22);
    let differentYear = currentDate - date;

    return new Date(differentYear).getFullYear() - unixZeroYear;
}

function getWeekDay(date) {
    const dateObj = new Date(date);

    return dateObj.toLocaleString('en-us', {weekday: 'long'});
}

function getAmountDaysToNewYear() {
    const currentDate = new Date();
    const newYear = currentDate.getFullYear() + 1;
    const nextNewYear = new Date(newYear, 0, 1);
    const msPerDay = 24 * 60 * 60 * 1000;

    return Math.round((nextNewYear - currentDate) / msPerDay);
}

function getProgrammersDay(inputYear) {
    const programmersDay = new Date(inputYear, 0, 256);
    const year = programmersDay.getFullYear();
    const month = programmersDay.toLocaleString('en-US', {month: 'short'});
    const date = programmersDay.getDate();
    const weekDay = getWeekDay(programmersDay);

    return `${date} ${month}, ${year} (${weekDay})`;
}

function howFarIs(specifiedWeekday) {
    const weekArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const currentDate = new Date();
    const weekDay = currentDate.getDay();
    const weekDayCapital = specifiedWeekday[0].toUpperCase() + specifiedWeekday.slice(1);
    const indexWeekDayInput = weekArray.indexOf(weekDayCapital);

    if (weekDay > indexWeekDayInput) {
        let leftWeekDay = weekDay - indexWeekDayInput;
        return `It's ${leftWeekDay} day(s) left till ${weekDayCapital}`;
    } else if (weekDay === indexWeekDayInput) {
        return `Hey, today is ${weekDayCapital} =)`;
    } else {
        let leftWeekDay = indexWeekDayInput - weekDay;
        return `It's ${leftWeekDay} day(s) left till ${weekDayCapital}`;
    }
}

function isValidIdentifier(str) {
    const regex = /^[a-z_$][a-z0-9_$]*$/i;

    return regex.test(str);
}

function capitalize(str) {
    const regex = /\b[a-z]/g;

    return str.replace(regex, c => c.toUpperCase());
}

function isValidAudioFile(str) {
    const regex = /^[a-zA-Z]+\.(mp3|flac|alac|aac)$/;

    return regex.test(str);
}

function getHexadecimalColors(str) {
    const regex = /#([a-f0-9]{3}){1,2}\b/ig;

    return str.match(regex);
}

function isValidPassword(str) {
    const regex = /(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d]{8,}/;

    return regex.test(str);
}

function addThousandsSeparators(num) {
    const regex = /\B(?=(\d{3})+(?!\d))/g;

    return num.toString().replace(regex, ',');
}

function getAllUrlsFromText(str) {
    const regex = /(http:|https:)\/\/[a-z\\.]+?\//ig;

    return str.match(regex);
}
