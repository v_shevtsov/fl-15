function getEventName() {
    const eventName = prompt('Enter event name', 'meeting');
    const form = document.querySelector('#form');
    const confirmBtn = document.querySelector('#btn-submit');
    const convertBtn = document.querySelector('#btn-convert');

    if (eventName) {
        showForm();

        form.addEventListener('submit', (e) => {
            e.preventDefault();
        });
        confirmBtn.addEventListener('click', showFormMessage.bind(null, eventName));
        convertBtn.addEventListener('click', convertCurrency);
    }
}

function showForm() {
    const form = document.querySelector('#form');

    form.style.display = 'flex';
}

function isEmptyInput(inputSelector) {
    const inputName = document.querySelector(inputSelector);
    const value = inputName.value;

    return value === '';
}

function isValidInputTime(inputSelector) {
    const regex = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
    const inputTimeValue = document.querySelector(inputSelector).value;

    return regex.test(inputTimeValue);
}

function isEmptyForm() {
    const isEmptyName = isEmptyInput('#input-name');
    const isEmptyTime = isEmptyInput('#input-time');
    const isEmptyPlace = isEmptyInput('#input-place');

    return !isEmptyName
        && !isEmptyTime
        && !isEmptyPlace;
}

function showFormMessage(eventName) {
    const isValidTime = isValidInputTime('#input-time');
    const nameValue = document.querySelector('#input-name').value;
    const timeValue = document.querySelector('#input-time').value;
    const placeValue = document.querySelector('#input-place').value;

    if (!isEmptyForm()) {
        alert('Input all data');
        return;
    }

    if (!isValidTime) {
        alert('Enter time in format hh:mm');
        return;
    }

    console.log(`${nameValue} has a ${eventName} at ${timeValue} somewhere in ${placeValue}`);
}

function convertCurrency() {
    const amountEUR = prompt('Enter amount in euro', '');
    const amountUSD = prompt('Enter amount in dollar', '');
    const currentCourseEUR = 33;
    const currentCourseUSD = 27.69;
    const numbersAfterDot = 2;

    if (amountEUR > 0 && amountUSD > 0) {
        const UERtoUAH = (amountEUR * currentCourseEUR).toFixed(numbersAfterDot);
        const USDtoUAH = (amountUSD * currentCourseUSD).toFixed(numbersAfterDot);

        alert(`${amountEUR} euros are equal ${UERtoUAH}hrns, ${amountUSD} dollars are equal ${USDtoUAH}hrns`);
    } else {
        alert('Please enter a valid amount');
        convertCurrency();
    }
}

getEventName();