const isEquals = (a, b) => a === b;

const isBigger = (a, b) => a > b;

const storeNames = (...rest) => rest;

const getDifference = (a, b) => a > b ? a - b : b - a;

const negativeCount = array => array.filter(el => el < 0).length;

const letterCount = (string, substring) => {
    let count = 0;
    for (let letter of string) {
        if (letter.toLowerCase() === substring.toLowerCase()) {
            count++;
        }
    }

    return count;
}

const countPoints = array =>
    array
        .reduce((accum, match) => {
            let [pointX, pointY] = match.split(':');

            if (+pointX > +pointY) {
                accum += 3;
            } else if (+pointX === +pointY) {
                accum += 1;
            }

            return accum;
        }, 0)
