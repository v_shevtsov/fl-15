function reverseNumber(num) {
    const numString = String(num);
    let result = '';

    const iterationWithMinus = num < 0 ? 1 : 0;

    for (let i = numString.length; i > iterationWithMinus; i--) {
        result += numString[i - 1];
    }

    return num < 0 ? -result : result;
}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i], i, arr);
    }
}

function map(arr, func) {
    const newArray = [];

    forEach(arr, (arr) => {
        newArray.push(func(arr));
    })

    return newArray;
}

function filter(arr, func) {
    const newArray = [];

    forEach(arr, (arr) => {
        if (func(arr)) {
            newArray.push(arr);
        }
    })

    return newArray;
}

function getAdultAppleLovers(data) {
    const minYear = 18;
    const resultObj = filter(data, (data) => {
        return data.age >= minYear && data.favoriteFruit === 'apple'
    })

    return map(resultObj, (resultObj) => {
        return resultObj.name;
    })
}

function getKeys(obj) {
    const keys = [];

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            keys.push(key);
        }
    }

    return keys;
}

function getValues(obj) {
    const keys = [];

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            keys.push(obj[key]);
        }
    }

    return keys;
}

function showFormattedDate(dateObj) {
    const monthStart = 4;
    const monthEnd = 7;
    const date = dateObj.getDate();
    const month = dateObj.toDateString().slice(monthStart, monthEnd);
    const year = dateObj.getFullYear();

    return `It is ${date} of ${month}, ${year}`;
}
