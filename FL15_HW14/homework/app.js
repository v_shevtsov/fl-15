const appRoot = document.getElementById('app-root');

appRoot.innerHTML = `
<header class="header">
<h1>Countries Search</h1>
<div class="header__choose">
<div class="choose__radio">
<p>Please choose type of search:</p>
<div class="radio-label">
<label>
<input type="radio" id="choose-region" name="radio-type">
By region
</label>
<label>
<input type="radio" id="choose-language" name="radio-type">
By Language
</label>
</div>
</div>
<div class="choose__select">
<label for="country">
Please choose search query:
<select id="select" disabled>Select value</select>
</label>
</div>
</div>
</header>
`;

const regionRadio = document.getElementById('choose-region');
const languageRadio = document.getElementById('choose-language');
const select = document.getElementById('select');

regionRadio.addEventListener('click', getRegionSelect);
languageRadio.addEventListener('click', getLanguageSelect);
select.addEventListener('change', createTable.bind(null, '#app-root'), {once: true});

function getLanguageSelect() {
    const select = document.querySelector('#select');
    select.disabled = false;
    const languageList = externalService.getLanguagesList();

    setOptionSelect(select, languageList);
}

function getRegionSelect() {
    const select = document.querySelector('#select');
    select.disabled = false;
    const regionList = externalService.getRegionsList();

    setOptionSelect(select, regionList);
}

function setOptionSelect(select, optionList) {
    clearChildren(select);
    setDefaultOption(select);

    for (let value of optionList) {
        const option = document.createElement('option');
        option.value = value;
        option.textContent = value;

        select.append(option);
    }
}

function setDefaultOption(select) {
    const defaultOption = document.createElement('option');
    defaultOption.textContent = 'Select value';
    defaultOption.value = 'Select value';
    select.append(defaultOption);
}

function clearChildren(container) {
    const children = container.children;

    for (let child of children) {
        child.remove();
    }
}

function createTable(containerSelector) {
    const container = document.querySelector(containerSelector);
    const table = document.createElement('table');
    table.className = 'table';

    createTableHeaders(table);

    container.append(table);
    select.addEventListener('change', event => {

        let type;

        if (regionRadio.checked) {
            type = regionRadio.id;
        }
        if (languageRadio.checked) {
            type = languageRadio.id
        }
        includeTable(type, event);
    });
}

function createTableHeaders(table) {
    const thead = document.createElement('thead');
    const headerRow = document.createElement('tr');
    const headers = ['Country name', 'Capital', 'World Region', 'Languages', 'Area', 'Flag'];
    headerRow.className = 'table-header';

    for (let i = 0; i < headers.length; i++) {
        const th = document.createElement('th');
        th.textContent = headers[i];

        headerRow.append(th);
    }
    thead.append(headerRow)
    table.prepend(thead);
}

function includeTable(type, event) {
    const value = event.target.value;
    const table = document.querySelector('.table');
    const nameArray = ['name', 'capital', 'region', 'languages', 'area', 'flagURL'];
    let arrayList;
    let row;

    clearChildren(table);
    createTableHeaders(table);

    if (type === 'choose-region') {
        arrayList = externalService.getCountryListByRegion(value);
    }

    if (type === 'choose-language') {
        arrayList = externalService.getCountryListByLanguage(value)
    }

    for (let i = 0; i < arrayList.length; i++) {
        row = document.createElement('tr');

        for (let name of nameArray) {
            const td = document.createElement('td');

            if (name === 'languages') {
                td.textContent = Object.values(arrayList[i][name]);

                row.append(td);
                continue;
            }

            if (name === 'flagURL') {
                td.innerHTML = `<img src="${arrayList[i][name]}" alt="flag">`;
                row.append(td);
                continue;
            }

            td.textContent = arrayList[i][name];

            row.append(td);
        }
    table.append(row);
    }
}




