function initGame() {
    const answer = confirm('Do you want to play a game?');
    const maxNumber = 8;
    const coefficient = 1;
    const totalPrize = 0;

    if (answer) {
        const result = isNumbersMatched(maxNumber, coefficient, totalPrize);
        showResultsMessage(result);
    } else {
        alert('You did not become a billionaire, but can.');
    }
}

function isValidUserNumber(userNumber, minNumber, maxNumber) {
    return userNumber >= minNumber && userNumber <= maxNumber;
}


function randomNumber(min, max) {
    const randomNumber = min + Math.random() * (max + 1 - min);

    return Math.floor(randomNumber);
}

function getPrize(attempt, coefficient = 1) {
    const firstAttemptPrize = 100;
    const secondAttemptPrize = 50;
    const thirdAttemptPrize = 25;
    const firstAttempt = 1;
    const secondAttempt = 2;
    const thirdAttempt = 3;

    let prize;

    if (attempt === firstAttempt) {
        prize = firstAttemptPrize * coefficient;
        return prize;
    }

    if (attempt === secondAttempt) {
        prize = secondAttemptPrize * coefficient;
        return prize;
    }

    if (attempt === thirdAttempt) {
        prize = thirdAttemptPrize * coefficient;
        return prize;
    }
}

function isNumbersMatched(maxNumber, coefficient, totalPrize) {
    const minNumber = 0;
    const maxAttempts = 3;
    const systemNumber = randomNumber(minNumber, maxNumber);

    for (let i = 0; i < maxAttempts; i++) {
        const attempts = maxAttempts - i;
        const currentPrize = getPrize(i + 1, coefficient);
        const userNumber = getUserNumber(minNumber, maxNumber, attempts, totalPrize, currentPrize);
        if (systemNumber === userNumber) {
            return {
                matched: true,
                coefficient: ++coefficient,
                totalPrize: totalPrize + currentPrize
            };
        }
    }
    return {
        matched: false,
        coefficient,
        totalPrize
    };
}

function getUserNumber(minNumber, maxNumber, attempts, totalPrize, currentPrize) {
    const userNumber = +prompt(`
    Choose a roulette pocket number from ${minNumber} to ${maxNumber}
    Attempts left: ${attempts}
    Total prize: ${totalPrize}
    Possible prize on current attempt: ${currentPrize} $
     `, '');
    const isValidNumber = isValidUserNumber(userNumber, minNumber, maxNumber);

    if (isValidNumber) {
        return userNumber;
    } else {
        alert('Number is not valid! Try again!');
        getUserNumber(minNumber, maxNumber, attempts, totalPrize, currentPrize);
    }
}

function showResultsMessage(result, restart) {
    if (result.matched) {
        restart = confirm(`Congratulation, you won! Your prize is: ${result.totalPrize} $. Do you want to continue?`);
    }

    restartGame(result, restart)
}

function restartGame(result, restart) {
    if (restart) {
        const maxNumber = 12;
        result = isNumbersMatched(maxNumber, result.coefficient, result.totalPrize);
        showResultsMessage(result);
    } else {
        alert(`Thank you for your participation. Your prize is: ${result.totalPrize} $`);
        restart = confirm(`Do you want to play again?`);
        if (restart) {
            showResultsMessage({
                    coefficient: result.coefficient,
                    totalPrize: result.totalPrize,
                    matched: false
                },
                restart)
        }
    }
}


initGame();
