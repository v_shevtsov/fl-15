function getPromptUser() {
    const amount = +prompt('Enter amount', '');
    const years = parseInt(prompt('Enter number of years', ''), 10);
    const percentage = +prompt('Enter percentage', '');
    const totalAmount = getTotalAmount(amount, years, percentage);
    const totalProfit = getTotalProfit(totalAmount, amount);

    showResults(amount, years, percentage, totalProfit, totalAmount);
}

function isValidPrompts(amount, years, percentage) {
    return isValidAmount(amount)
        && isValidYears(years)
        && isValidPercentage(percentage);
}

function isValidAmount(amount) {
    const maxAmount = 1000;

    return amount >= maxAmount;
}

function isValidYears(years) {
    return years >= 1;
}

function isValidPercentage(percentage) {
    const minPercentage = 0;
    const maxPercentage = 100;

    return percentage >= minPercentage && percentage <= maxPercentage;
}

function getTotalAmount(amount, years, percentage) {
    const numbersAfterDot = 2;
    const maxPercentage = 100;
    let totalAmount = amount;

    for (let i = 0; i < years; i++) {
        totalAmount += totalAmount * (percentage / maxPercentage);
    }

    return totalAmount.toFixed(numbersAfterDot);
}

function getTotalProfit(totalAmount, amount) {
    const numbersAfterDot = 2;
    return (totalAmount - amount).toFixed(numbersAfterDot);
}

function showResults(amount, years, percentage, totalProfit, totalAmount) {
    const isValid = isValidPrompts(amount, years, percentage);

    const message = isValid
        ? `
    Initial amount: ${amount}
    Number of years: ${years}
    Percentage of year: ${percentage}

    Total profit: ${totalProfit}
    Total amount: ${totalAmount}`
        : 'Invalid input data';

    alert(message);
}


getPromptUser();


