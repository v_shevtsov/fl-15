/* START TASK 1: Your code goes here */

function changeTableColor() {
    const table = document.querySelector('.table');

    table.addEventListener('click', e => {
        e.target.bgColor = 'yellow';
        e.target.parentNode.bgColor = 'blue';

        if (e.target.className === 'special-cell') {
            e.currentTarget.bgColor = 'green';
        }
    })
}

changeTableColor();

/* END TASK 1 */

/* START TASK 2: Your code goes here */

function checkInputTel() {
    const input = document.querySelector('.input-tel');

    input.addEventListener('change', () => showMessage(input))
}

function isValidTel(input) {
    const value = input.value;
    const regex = /^(\+380)[0-9]{9}$/;

    return regex.test(value);
}

function sendTel(e, input, messageContainer) {
    e.preventDefault();
    input.value = '';
    messageContainer.textContent = 'Data was successfully sent';
    messageContainer.classList.add('success-message');
}

function showMessage(input) {
    const messageContainer = document.querySelector('.message');
    const btn = document.querySelector('.btn-send');
    const isValid = isValidTel(input);

    if (input.value.length === 0) {
        messageContainer.textContent = '';
        messageContainer.className = 'message';
        return;
    }

    if (isValid) {
        btn.disabled = false;
        messageContainer.classList.remove('error-message');
        messageContainer.textContent = '';
        input.classList.remove('error-input');

        btn.addEventListener('click', (e) => sendTel(e, input, messageContainer));
    } else {
        messageContainer.textContent = 'Type number does not follow format +380*********';
        messageContainer.classList.add('error-message');
        input.classList.add('error-input');
        btn.disabled = true;
    }

}


checkInputTel();

/* END TASK 2 */

/* START TASK 3: Your code goes here */

const court = document.querySelector('#task3');
const ball = document.querySelector('.ball');
const score = {
    commandX: 0,
    commandY: 0
};
court.addEventListener('click', e => moveBall(e));

function moveBall(e) {
    const courtCoordinates = e.currentTarget.getBoundingClientRect();
    const ballCoords = {
        top: e.clientY - courtCoordinates.top - court.clientTop - ball.clientHeight / 2,
        left: e.clientX - courtCoordinates.left - court.clientLeft - ball.clientWidth / 2
    };

    if (ballCoords.top < 3) {
        ballCoords.top = 3;
    }

    if (ballCoords.left < 3) {
        ballCoords.left = 3;
    }

    if (ballCoords.left + ball.clientWidth > court.clientWidth) {
        ballCoords.left = court.clientWidth - ball.clientWidth - 3;
    }

    if (ballCoords.top + ball.clientHeight > court.clientHeight) {
        ballCoords.top = court.clientHeight - ball.clientHeight - 9;
    }

    ball.style.left = ballCoords.left + 'px';
    ball.style.top = ballCoords.top + 'px';

    if (e.target.className === 'left-zone') {
        ++score.commandX;
        showScore();
    }

    if (e.target.className === 'right-zone') {
        ++score.commandY;
        showScore();
    }
}

function showScore() {
    const notification = document.querySelector('.notification-score');
    notification.textContent = `CommandX: ${score.commandX}, CommandY: ${score.commandY}`;

    setTimeout(() => {
        notification.textContent = '';
    }, 3000);
}

/* END TASK 3 */


