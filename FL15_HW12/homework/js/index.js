function visitLink(path) {
    let countVisit = localStorage.getItem(path) || 0;
    countVisit++;
    localStorage.setItem(path, countVisit);
}

function viewResults() {
    const container = document.querySelector('.container');
    const numberOfPages = 3;
    const list = document.createElement('ul');
    const oldList = document.querySelector('.visited-list');

    if (oldList) {
        oldList.remove();
    }

    list.className = 'visited-list';

    for (let i = 1; i <= numberOfPages; i++) {
        let visitedPage = localStorage.getItem('Page' + i);

        if (!visitedPage) {
            continue;
        }

        const message = `You visited Page${i} ${visitedPage} time(s)`;
        const li = document.createElement('li');
        li.textContent = message;
        list.append(li);

    }

    container.append(list);

    localStorage.clear();
}

